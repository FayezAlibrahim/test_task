<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Country;

class Company extends Model
{
    use HasFactory;

    public function country()
    {
        return $this->hasOne(Country::class);
    } 

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
