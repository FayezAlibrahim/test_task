<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Country;
use App\Models\Company;
class UserController extends Controller
{
    public function getUsersCompaniesByCountry($country=null){
        //Define country obj
        $country =isset($country)?$country:'Canada';
        //get country id
        $Country_id=Country::where('name','like','%'.$country.'%')->first()->id;
        //retrive users of these companies
        $users=User::with('companies')->has('companies')->get()->toArray();
// how to show..
        foreach ($users as $user){
        dd($user['id'],$user['name'],$user['companies'][0]["name"],$user['companies'][0]["pivot"]["created_at"]);
        }
// note: i'm  showing here the data of the first company related to this
// user (for testing purposes only), if we have view page all we need to do is looping through $user["companies"] array  
        

// return view or as json response here....
    }
}
